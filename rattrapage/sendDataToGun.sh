#!/bin/bash

# Authour : KHELIFI Charif
# Date : 23092016
# Send data visual to GUN for treatment
# 1 parametre : l'alias du serveur RPU (ou IP)

# Variable
lftp=/usr/bin/lftp
rsync=/usr/bin/rsync
RPU=$1
#RPU=taris
DATE=`date +%d""%m""%Y""%H""%M`
REP_LOG=/data/gun/log
FILE_LOG=$REP_LOG/sendDataToGun.$DATE.log
REP_TMP=/data/media/tmpRPU/
liste_img=""
MAIL_LIST="ckhelifi@galerieslafayette.com, mhabibi@galerieslafayette.com"
set +x

FTP=84.14.197.11 # ftp2.gutenberg-networks.com
login=GL-img,gal57img
SRCDIR=/appli/hybris/impex/export/data/outgoing/gun/media/
ARCDIR=/appli/hybris/impex/export/data/archive/gun/media/

FILE_OUT=/appli/gun/scripts/data_send.json
FILE_LOCAL_DIF=/appli/gun/scripts/local_dir.txt
FILE_LOCAL_DIF_TMP=/appli/gun/scripts/local_dir.txt.2
FILE_LFTP_DIF=/appli/gun/scripts/lftp_dir.txt
FILE_LFTP_DIF_TMP=/appli/gun/scripts/lftp_dir.txt.2

# Save stdout and stderr
exec 6>&1
exec 7>&2

# Redirection stdout et stderr into FILELOG
exec >> $FILE_LOG
exec 2>> $FILE_LOG

echo "############ $(date +%H:%M) ##########"

# Controle des variables en entree du script
if [ $# -ne 1 ]; then
        echo "Erreur !! Le script a besoin d'un argument :"
        echo "-Alias serveur RPU / adresse IP"
        echo "./getDataFromGun.sh <alias_ou_IP_RPU>"
        exit 1;
fi

# Creation directories used by this script if inexistant 
#tmp
if [ ! -d $REP_TMP ]; then
	echo "Creating tempory working local directory $REP_TMP"
	mkdir $REP_TMP
fi
#Log
if [ ! -d $REP_LOG ];then
	echo "Creating log directory $REP_LOG"
	mkdir $REP_LOG
fi
# SRCDIR
ssh $RPU "ls -lrt $SRCDIR" > /dev/null 2>&1
if [ $? -ne 0 ]; then 
	echo "Creating distant directory $RPU:$SRCDIR"
	ssh $RPU "mkdir -p $SRCDIR"
fi
# ARCDIR
ssh $RPU "ls -lrt $ARCDIR" > /dev/null 2>&1
if [ $? -ne 0 ]; then 
	echo "Creating distant archive directory $RPU:$ARCDIR"
	ssh $RPU "mkdir -p $ARCDIR"
fi

# Synchro source dir to tempory working dir
echo "Synchronize distant directory $RPU:$SRCDIR with local tempory working directory $REP_TMP"
echo "$rsync -azv $RPU:$SRCDIR $REP_TMP"
$rsync -azv $RPU:$SRCDIR $REP_TMP

# Report all files to transfert
echo "ls -l $REP_TMP > $FILE_LOCAL_DIF"
ls -l $REP_TMP > $FILE_LOCAL_DIF
while read line
do
	new_line=`echo $line | cut -d" " -f9 | sed "s/\ //g"`
	new_line="$new_line `echo $line | cut -d' ' -f5 | sed 's/\ //g'`"
	sed "s/$line/$new_line/g" $FILE_LOCAL_DIF > $FILE_LOCAL_DIF_TMP
	mv $FILE_LOCAL_DIF_TMP $FILE_LOCAL_DIF
done < $FILE_LOCAL_DIF
sed '1d' $FILE_LOCAL_DIF > $FILE_LOCAL_DIF_TMP
mv $FILE_LOCAL_DIF_TMP $FILE_LOCAL_DIF
TEST_LAST_LINE=`sed -n '$p' $FILE_LOCAL_DIF`
if [[ "$TEST_LAST_LINE" =~ "^media" ]]; then
	sed '$d' $FILE_LOCAL_DIF > $FILE_LOCAL_DIF_TMP
	mv $FILE_LOCAL_DIF_TMP $FILE_LOCAL_DIF
fi

if [ `cat $FILE_LOCAL_DIF | wc -l` -eq 0 ];then
	echo "No data to send into $RPU:$SRCDIR"
	echo "exit"
	exit 0
fi

# Send files to ftp
echo "Connect and send data visuals to Gun"
echo "FTP : $FTP"
echo "$lftp -u $login $FTP -e 'set ftp:ssl-allow no;set net:reconnect-interval-base 5; set net:max-retries 2; lcd /data/media/tmpRPU/ ; mirror -vvv -R -n -c --depth-first --Remove-source-files --parallel=3 ; exit '"
$lftp -u $login $FTP -e 'set ftp:ssl-allow no;set net:reconnect-interval-base 5; set net:max-retries 2; lcd /data/media/tmpRPU/ ; mirror -vvv -R -n -c --depth-first --Remove-source-files --parallel=3 ; exit '

# Check return code
CR=$?
test $CR -ne 0 && cat $FILE_LOG | mail -s '[sendDataToGun.sh] Error when sending data files to FTP GUN' $MAIL_LIST

# Compare this new file with the real status of the ftp directory
echo "$lftp -u $login $FTP -e 'dir ; exit' > $FILE_LFTP_DIF"
$lftp -u $login $FTP -e 'dir ; exit' > $FILE_LFTP_DIF
while read line
do
	new_line=`echo $line | cut -d" " -f9 | sed "s/\ //g"`
	new_line="$new_line `echo $line | cut -d' ' -f5 | sed 's/\ //g'`"
	sed "s/$line/$new_line/g" $FILE_LFTP_DIF > $FILE_LFTP_DIF_TMP
	mv $FILE_LFTP_DIF_TMP $FILE_LFTP_DIF
done < $FILE_LFTP_DIF
TEST_LAST_LINE=`sed -n '$p' $FILE_LFTP_DIF`
if [[ "$TEST_LAST_LINE" =~ "^media" ]]; then
	sed '$d' $FILE_LFTP_DIF > $FILE_LFTP_DIF_TMP
	mv $FILE_LFTP_DIF_TMP $FILE_LFTP_DIF
fi

echo "cat $FILE_LFTP_DIF"
echo "`cat $FILE_LFTP_DIF`"
echo "cat $FILE_LOCAL_DIF"
echo "`cat $FILE_LOCAL_DIF`"

echo "Making list of files successfully transfered"
echo "Compare files to transfer to files on ftp"
echo "Compare $FILE_LOCAL_DIF and $FILE_LFTP_DIF"
CR=0
while read line
do
	COMPARE=FALSE
	my_name=`echo $line | cut -d" " -f1 | sed "s/\ //g"`
	my_lenght=`echo $line | cut -d" " -f2 | sed "s/\ //g"`
	while read line_lftp
	do
		my_name_lftp=`echo $line_lftp | cut -d" " -f1 | sed "s/\ //g"`
		my_lenght_lftp=`echo $line_lftp | cut -d" " -f2 | sed "s/\ //g"`
		if [ "$my_name" == "$my_name_lftp" ];then
			if [ "$my_lenght" == "$my_lenght_lftp" ];then
				echo "This file ${my_name} has been correctly found on the ftp $FTP"
				COMPARE=TRUE
				echo "Adding $my_name"
				
				if [[ $my_name =~ [1-9]*_[1-9]+.[a-zA-Z]* ]];then
					my_name=`echo $my_name | cut -d"." -f1 | sed "s/\ //g"`
					if [ "$liste_img" == "" ];then
						liste_img="\"$my_name\""
					else
						liste_img="${liste_img},\"${my_name}\""
					fi
				else
					echo "This file ${my_name} doen't match with the media's regex : [1-9]*_[1-9]+.[a-zA-Z]*"
				fi
				
			fi
		fi
	done < $FILE_LFTP_DIF
	if [ "$COMPARE" == "FALSE" ];then
		echo "The file ${line} hasn't been correctly transfered on the ftp $FTP"
		CR=3
	fi
done < $FILE_LOCAL_DIF
test $CR -ne 0 && cat $FILE_LOG | mail -s '[sendDataToGun.sh] Error when sending data files to FTP GUN' $MAIL_LIST

if [ -z $liste_img ];then
	echo "No data correctly transfered to call webservice checkgunexport."
	echo "exit"
	exit 0
fi

echo "[$liste_img]" > $FILE_OUT
echo "File successfully transfered : $liste_img"
echo "cat $FILE_OUT :"
echo "`cat $FILE_OUT`"

# Call WS rpu
echo "Calling Webservice checkgunexport with json file : $FILE_OUT"
curl -H "Content-type: application/json" -X POST --data "@${FILE_OUT}" "http://${RPU}:9001/glpcmwebservices/checkgunexport"
if [ $? -ne 0 ];then
	echo "An error occured when calling webservice checkgunexport"
	cat $FILE_LOG | mail -s "[sendDataToGun.sh] Error when calling webserice checkgunexport" $MAIl_LIST
	exit 2;
fi

# Purge tempory working dir
echo "Purge tempory working dir $REP_TMP"
rm -rf $REP_TMP* > /dev/null 2>&1

# Archivage
if [ $CR -eq 0 ];then
	echo "Archive data visuals into $RPU:$ARCDIR"
	max=`echo $liste_img | grep -o ',' | wc -l`
	max=$(($max+1))
	my_imgs=""
	for (( i=1; i<=$max; i++ ))
	do
		full_path_img="${SRCDIR}`echo ${liste_img} | cut -d',' -f${i} | sed 's/\"//g'`*"
		if [ "$my_imgs" == "" ];then
			my_imgs=$full_path_img
		else
			my_imgs="${my_imgs} $full_path_img"
		fi
	done
	echo "ssh  $RPU \"mv $my_imgs $ARCDIR\""
	ssh $RPU "mv $my_imgs $ARCDIR"
fi

# Restauration stdout and stderr
exec 1>&6 6>&-
exec 2>&7 7>&-

exit $CR