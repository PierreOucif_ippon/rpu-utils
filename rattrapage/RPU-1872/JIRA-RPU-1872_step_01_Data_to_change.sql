-- Retrouver tous les articles concernÚs par l'utilisation de la teinte TINT-963
-- Pour la mise au point, utiliser la clause select suivante : select P.CODE as PRODUCT_CODE, CA.P_CODE as CLASS_ATTR, CAV.P_CODE as CLASS_ATTR_VAL, CAV.PK as CAV_PK, C.P_ID as CLASSIFICATION_SYSTEM, CV.P_VERSION as CLASS_SYST_VERSION
select ';' || P.CODE || ';glpcmClassification/1.0/CL-TINT.beauty-tint;fr;enum,' || C.P_ID || ',' || CV.P_VERSION || ',TINT-378'
from PRODUCTFEATURES PF

join CAT2ATTRREL C2AR on PF.P_CLASSIFICATIONATTRIBUTEASSIG = C2AR.PK
join CLASSIFICATIONATTRS CA on CA.PK = C2AR.P_CLASSIFICATIONATTRIBUTE
join ATTR2VALUEREL A2VR on A2VR.P_ATTRIBUTE = CA.PK
join CLASSATTRVALUES CAV on CAV.PK = A2VR.P_VALUE
join CATALOGVERSIONS CV on CV.PK = CAV.P_SYSTEMVERSION
join CATALOGS C on C.PK = CV.P_CATALOG

join PRODUCTS P on P.PK = PF.P_PRODUCT

where CAV.P_CODE = 'TINT-963' and PF.P_QUALIFIER = 'glpcmClassification/1.0/CL-TINT.beauty-tint' and CA.P_CODE = 'BEAUTY-TINT' and TO_NUMBER(PF.P_STRINGVALUE) = CAV.PK

order by P.CODE;
