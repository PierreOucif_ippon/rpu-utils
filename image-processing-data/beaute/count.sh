#!/bin/bash
declare -i i=0

for line in $(cat list.txt)
do 
	if ! grep -q $line list2.txt; then
		i=$(($i+1))
		echo $line >> diff_visu22.txt
	fi
done
echo $i
