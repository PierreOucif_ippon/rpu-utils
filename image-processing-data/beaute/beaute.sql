select distinct rmsaexport.p_ug as ug_article from ENRICHWFCTX ewc
left join ENRICHWFCTX2ART rel on rel.SOURCEPK=ewc.pk
left join PRODUCTS article on article.pk = rel.TARGETPK
left join PRODUCTS aexport on aexport.pk = ewc.P_ARTICLEFORARTICLEDATAEXPORT
left join OFFERS oexport on oexport.p_article = aexport.pk
left join rmsarticles rmsaexport on rmsaexport.p_ug = oexport.p_code
left join OFFERS o on o.P_ARTICLE = article.pk
left join RMSARTICLES rmsa on rmsa.p_ug = o.p_code
left join RMSPRODUCTS rmsp on rmsp.pk = rmsa.p_product
left join PRODUCTS product on product.pk = article.P_BASEPRODUCT
left join CAT2PRODREL C2P on C2P.TARGETPK = product.PK
left join CATEGORIES CAT on C2P.SOURCEPK = CAT.PK
left join CATEGORIESLP CATLP on CATLP.itempk = CAT.PK
left join COMPOSEDTYPES ITEMTYPE on ITEMTYPE.pk = CATLP.itemtypepk
left join CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
left join CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
left join CAT2CATREL TOPC2C on TOPC2C.TARGETPK = C2C.SOURCEPK
left join CATEGORIES TOPCAT on TOPCAT.PK = TOPC2C.SOURCEPK
left join CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
left join CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
left join CATEGORIES UNIVCAT on UNIVCAT.PK = UNIVC2C.SOURCEPK
left join CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
where UNIVCAT.P_CODE not like 'CL%'and CAT.P_CODE not like '%CLA%' AND PARENTCAT.P_CODE not like '%CLA%'
and UNIVCAT.P_CODE = 'PU_1'
;


-- rmsArticle.ug
select ewc.P_ARTICLEFORARTICLEDATAEXPORT from ENRICHWFCTX ewc;
select rmsa.P_UG as rmsArticle_ug
             , rmsp.P_UG as rmsProduct_ug 
             , rmsa.p_ean as rmsArticle_ean
             , rmsa.P_SEASON as rmsArticle_season
             , cav.P_CODE code_main_classif
             , cavlp.p_name name_main_classif
from RMSARTICLES rmsa
join offers o on o.P_CODE = rmsa.P_UG
join RMSPRODUCTS rmsp on rmsp.pk = rmsa.P_PRODUCT
join PRODUCTS article on o.P_ARTICLE = article.pk
join ENRICHWFCTX ewc on ewc.P_ARTICLEFORARTICLEDATAEXPORT = article.pk
join ENRICHWFCONF ewconf on ewconf.pk = ewc.P_ENRICHMENTWORKFLOWCONFIGURAT
join PRODUCTS product on product.pk = article.P_BASEPRODUCT
left join productfeatures pf on pf.p_product = article.pk
left join CLASSATTRVALUES cav on cav.pk = cast(pf.P_STRINGVALUE as varchar2(3999))
left join CLASSATTRVALUESLP cavlp on cavlp.ITEMPK = cav.pk
left join classvarianttypes cvt on cvt.pk = product.P_CLASSIFICATIONVARIANTTYPE
left join classificationattrs ca on ca.pk = cvt.P_MAINCLASSIFICATIONATTRIBUTE
where rmsa.p_ug = 98687239
and pf.P_STRINGVALUE like '8%'
and cav.p_code like concat(ca.p_code,'%')
;

select ewc.pk
             , ewc.P_ARTICLEDATAEXPORTDATE
             , ewc.P_GUNDEPOSITDATE
             , ewconf.p_code as code_enrichmentWFConfig
             , rmsa.P_UG as rmsArticle_ug
             , rmsp.P_UG as rmsProduct_ug 
             , rmsa.p_ean as rmsArticle_ean
             , rmsa.P_SEASON as rmsArticle_season
             , pf.P_STRINGVALUE
             , cav.p_code
from RMSARTICLES rmsa
join offers o on o.P_CODE = rmsa.P_UG
join RMSPRODUCTS rmsp on rmsp.pk = rmsa.P_PRODUCT
join PRODUCTS article on o.P_ARTICLE = article.pk
join ENRICHWFCTX ewc on ewc.P_ARTICLEFORARTICLEDATAEXPORT = article.pk
join ENRICHWFCONF ewconf on ewconf.pk = ewc.P_ENRICHMENTWORKFLOWCONFIGURAT
join PRODUCTS product on product.pk = article.P_BASEPRODUCT
left join productfeatures pf on pf.p_product = article.pk
left join CLASSATTRVALUES cav on cav.pk = cast(pf.P_STRINGVALUE as varchar2(3999))
left join CLASSATTRVALUESLP cavlp on cavlp.ITEMPK = cav.pk
left join classvarianttypes cvt on cvt.pk = product.P_CLASSIFICATIONVARIANTTYPE
left join classificationattrs ca on ca.pk = cvt.P_MAINCLASSIFICATIONATTRIBUTE
where pf.P_STRINGVALUE like '8%'
;


select * from RMSARTICLES rmsa
left join OFFERS o on o.p_code = rmsa.p_ug
left join PRODUCTS article on article.pk = o.p_article
left join PRODUCTS product on product.pk = article.P_BASEPRODUCT
left join CAT2PRODREL C2P on C2P.TARGETPK = product.PK
left join CATEGORIES CAT on C2P.SOURCEPK = CAT.PK
left join CATEGORIESLP CATLP on CATLP.itempk = CAT.PK
left join COMPOSEDTYPES ITEMTYPE on ITEMTYPE.pk = CATLP.itemtypepk
left join CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
left join CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
left join CAT2CATREL TOPC2C on TOPC2C.TARGETPK = C2C.SOURCEPK
left join CATEGORIES TOPCAT on TOPCAT.PK = TOPC2C.SOURCEPK
left join CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
left join CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
left join CATEGORIES UNIVCAT on UNIVCAT.PK = UNIVC2C.SOURCEPK
left join CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
where UNIVCAT.P_CODE not like 'CL%'and CAT.P_CODE not like '%CLA%' AND PARENTCAT.P_CODE not like '%CLA%'
and UNIVCAT.P_CODE = 'PU_1'
; 
