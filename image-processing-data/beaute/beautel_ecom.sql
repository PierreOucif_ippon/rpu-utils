Select Distinct count(Parentp.Code)

From GL_HYBRIS.Products Parentp
left outer join GL_HYBRIS.productslp PLP on Parentp.PK = PLP.ITEMPK
left outer join (
        select UNIVCATLP.P_NAME as GROUPE
            -- , UNIVCATLP.P_CODE as CodeGROUPE
             , PARENTCATLP.P_NAME as CAT
             , FAMPRO.PK as PRO
             , SUPP.P_NAME as STATUT_FRN
             , PARENTCAT.P_CODE as CODE_CAT
             from GL_HYBRIS.PRODUCTS FAMPRO
        left join GL_HYBRIS.CAT2PRODREL C2P on C2P.TARGETPK = FAMPRO.PK
        left join GL_HYBRIS.CATEGORIES CAT on C2P.SOURCEPK = CAT.PK
        left join GL_HYBRIS.CATEGORIESLP CATLP on CATLP.itempk = CAT.PK
        left join GL_HYBRIS.COMPOSEDTYPES ITEMTYPE on ITEMTYPE.pk = CATLP.itemtypepk
        left join GL_HYBRIS.CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
        left join GL_HYBRIS.CATEGORIESLP PARENTCATLP on PARENTCATLP.ITEMPK = C2C.SOURCEPK
        left join GL_HYBRIS.CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
        left join GL_HYBRIS.ENUMERATIONVALUESLP SUPP on SUPP.ITEMPK = PARENTCAT.P_SUPPLIERSTATUS
        left join GL_HYBRIS.CAT2CATREL TOPC2C on TOPC2C.TARGETPK = PARENTCATLP.ITEMPK
        left join GL_HYBRIS.CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
        left join GL_HYBRIS.CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
        left join GL_HYBRIS.CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
        where ITEMTYPE.P_SHORTID='RSF'
        ) RMS on RMS.PRO = PARENTP.P_BASEPRODUCT
left outer join (
        select UNIVCATLP.P_NAME UNIVERS
             , UNIVCAT.P_CODE code_univers
             , PARENTCATLP.P_NAME Sous_famille
             , PARENTCAT.P_CODE code_sfam
             , TOPCAT.P_CODE code_fam
             , TOPCATLP.P_NAME Famille
             , CAT.P_CODE code_ssfam
             , CATLP.P_NAME Sous_sous_famille
             , Fampro.Pk Pro
             , ca.P_Id ssfam_catalog
             , cv.P_VERSION ssfam_catalog_version
             from GL_HYBRIS.PRODUCTS FAMPRO
                left join GL_HYBRIS.CAT2PRODREL C2P on C2P.TARGETPK = FAMPRO.PK
                left join GL_HYBRIS.CATEGORIES CAT on C2P.SOURCEPK = CAT.PK
                left join GL_HYBRIS.CATEGORIESLP CATLP on CATLP.itempk = CAT.PK
                left join GL_HYBRIS.COMPOSEDTYPES ITEMTYPE on ITEMTYPE.pk = CATLP.itemtypepk
                left join GL_HYBRIS.CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
                left join GL_HYBRIS.CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
                left join GL_HYBRIS.CATEGORIESLP PARENTCATLP on PARENTCATLP.ITEMPK = C2C.SOURCEPK
                left join GL_HYBRIS.CAT2CATREL TOPC2C on TOPC2C.TARGETPK = PARENTCATLP.ITEMPK
                left join GL_HYBRIS.CATEGORIES TOPCAT on TOPCAT.PK = TOPC2C.SOURCEPK
                left join GL_HYBRIS.CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
                left join GL_HYBRIS.CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
                left join GL_HYBRIS.CATEGORIES UNIVCAT on UNIVCAT.PK = UNIVC2C.SOURCEPK
                left join GL_HYBRIS.CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
        left join GL_HYBRIS.Catalogs ca ON CAT.P_Catalog = ca.Pk
        left join GL_HYBRIS.CatalogVersions cv ON CAT.p_catalogVersion = cv.pk
        where ITEMTYPE.P_SHORTID='PSSF' AND CAT.P_CODE not like '%CLA%' AND PARENTCAT.P_CODE not like '%CLA%'
        ) Cat On Cat.Pro = Parentp.Pk
    left join GL_HYBRIS.Catalogs ca ON Parentp.P_Catalog = ca.Pk
    left join GL_HYBRIS.CatalogVersions cv ON Parentp.p_catalogVersion = cv.pk
Where PARENTP.P_BASEPRODUCT is null AND  PARENTP.p_catalogversion=8796093153881 and CAT.code_univers='PU_1';
