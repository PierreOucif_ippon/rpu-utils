#!/bin/bash

# Authour : KHELIFI Charif
# Date : 23022017	
# Rollback visual with timestamp or visual(UG+Coor)

# Variable
SRCARCHDIR=/data/media/app_archive

if [ $# -eq 0 ];then
	echo >&2 \
	"usage: $0 [-t timestamp] [-v visual] [-c color]"
	exit 1;
fi

while [ $# -gt 0 ]
do
    case "$1" in
        -t)  timestamp="$2"; shift; shift;;
		-v)  visual=$2; shift; shift;;
		-c)	 color=$2; shift; shift;;
    esac
done


# begin
echo "Beginning treatment"

# case timestamp
if [ "$timestamp" != "" ];then
	echo "Treatment with timestamp : $timestamp"
	echo "find $SRCARCHDIR/$timestamp/*/*/ -name '*.jpg'"
	find $SRCARCHDIR/$timestamp/*/*/ -name "*.jpg" | while read line;do 
		echo "Treatment $line :"
		color=`echo $line | cut -d'/' -f6`
		visual=`echo $line | cut -d'/' -f7`
		echo "mv $line /data/media/$color/$visual/"
		mv $line /data/media/$color/$visual/
		CR=$?
		if [ $CR -ne 0 ];then
			echo "Error moving $line"
		fi
	done
fi

# case color + UG
if [ "$visual" != "" ] && [ "$color" != "" ];then
	echo "Treatment with visual and color : $visual $color"
	echo "find $SRCARCHDIR/$timestamp/$color/$visual/ -name '*.jpg'"
	find $SRCARCHDIR/ -name "*.jpg" | grep $color | grep $visual | while read line;do 
		echo "Treatment $line :"
		echo "mv $line /data/media/$color/$visual/"
		mv $line /data/media/$color/$visual/
		CR=$?
		if [ $CR -ne 0 ];then
			echo "Error moving $line"
		fi
	done
fi

#End 
exit $CR
