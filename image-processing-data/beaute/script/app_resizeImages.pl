#!/usr/bin/perl
# MMO le 29/07/13
# ABS 11.03.2014 - Modif purge CDN
use strict;
use threads;
use threads::shared;
use Image::Magick;
use Getopt::Std;
use Data::Dumper;
use File::Type;

#Variables
my $ft = File::Type->new();
my (%options, %props, %img);
my @propsFile = qw(SRCSRV SRCDIR SRCARCHDIR DESTDIR PURGE VARNISHSRV HYBRISSRV LOCKSUFFIX);
my $date = `date +%d%m%Y`;
my $fulldate=`date +%d%m%Y:%H%M%S`;
my $fulldatearch=`date +%d%m%Y%H%M%S`;
chomp($fulldate);
chomp($date);
chomp($fulldatearch);

my $DESTARCHDIR="/data/media/app_archive/$fulldatearch";

chomp($fulldate);
chomp($date);
my $lock = "/tmp/resizeImages.lock";
my $log = "/appli/resize/log/app_resize.$date.log";
my $workingdir = "/appli/resize/visuels";
my ($varnishsrv, $hybrissrv, $hybrissrv_rpu);
my (@threads, @return, @ugprodCC);
share(@ugprodCC);
my $maxth = 5;
my $cdnpurge = "";
my ($command, $result);
share($cdnpurge);

my $requestDate = `date "+%a, %d %b %Y %H:%M:%S GMT" -u`;
chomp($requestDate);

my $env = "";
my $ressource = "/invalidations/v1.0/63546/BBDQ14308/static.galerieslafayette.com";
my $verb = "POST";
my $ct = "text/xml";
my $secretkey = "kzeZTbxhMpCGY8M6GDeJ";
my $hashValue = `echo -n -e "$requestDate\n$env$ressource\n$ct\n$verb\n" | openssl dgst -sha1 -hmac "$secretkey" -binary | base64`;
chomp($hashValue);
my $authstring = "MPA 161387138:".$hashValue;
my $url = "https://ws.level3.com/invalidations/v1.0/63546/BBDQ14308/static.galerieslafayette.com";

my $full_cc = "";

#Usage
sub print_usage {
        print "Usage :\n";
        print "./resizeImage.pl -p <property file>\n";
        print "-p : property - mandatory\n";
        exit(2);
}

#Check parameters
sub checkParam {
        getopts( "p:",\%options);
        print_usage() unless ($options{p});
}

#Check if already running
sub checkLock {
        if (-e $lock.$props{LOCKSUFFIX}) {
                print "Already running, exit\n";
                exit(1);
        }
}

#Check all props are ok
sub checkProps {

        #Check props
        for my $prop (@propsFile) {
                if ($props{$prop} !~ /.+/) {
                        print "Wrong property : $prop\n";
                        exit(1);

                }
        }
	$workingdir = "/appli/resize/".$props{LOCKSUFFIX};
}

#Load properties
sub loadProps {

        open(F, $options{p}) or die "Couldn't open file '$options{p}': $!";
        while (my $line = <F>) {
                $line =~ /(.*)=(.*)/;
                $props{$1} = $2;
        }
        close F;

        #Check properties
        checkProps();

        $varnishsrv = $props{VARNISHSRV};
        $hybrissrv = $props{HYBRISSRV};
        $hybrissrv_rpu = $props{HYBRISSRV_RPU};

}


sub resize {
        my $image = Image::Magick->new;
        $image->Read($_[0]);
        $image->Set(Gravity => 'Center');
        $image->Resize(geometry => "$_[2]x$_[1]");
        $image->Extent(geometry => "$_[2]x$_[1]");
        $image->Write(filename => $_[3], quality => 90);
}

sub purge_CDN {
	$requestDate = `date "+%a, %d %b %Y %H:%M:%S GMT" -u`;
	chomp($requestDate);
	$hashValue = `echo -n -e "$requestDate\n$env$ressource\n$ct\n$verb\n" | openssl dgst -sha1 -hmac "$secretkey" -binary | base64`;
	chomp($hashValue);
	$authstring = "MPA 161387138:".$hashValue;
        $command = 'curl -XPOST -d "<paths><path>'.$_[0]."</path></paths>\" $url -k -H Authorization:\"$authstring\" -H Date:\"$requestDate\" -H Content-Type:$ct";
        $result = `$command`;
	print $result . "\n";
	if ($result =~ /<invalidation id=/) {
        	print "Purge CDN OK for ".$_[0]."\n";
	} else {
		sleep 30;
		$result = `$command`;
		if ($result =~ /<invalidation id=/) {
        		print "Purge CDN OK for ".$_[0]."\n";
		} else {
			print "Purge CDN KO for ".$_[0]."\n";
		}
	}
}

sub purge_VARNISH {
        `ssh $varnishsrv "source ~/.bash_profile;/appli/varnish/bin/varnishpurge \".*G_$_[0].*\""`;
}

sub treatment_noCC {
        my $visuel = $_[0];
        my $prefix;
        $visuel =~ /(.{3})/;
        if ($visuel =~ /^3004/ && length($visuel) > 8) {
                $visuel =~ /(.{7})/;
                $prefix = $1;
        } else {
                $visuel =~ /(.{3})/;
                $prefix = $1;
        }

        foreach my $type (keys %{$img{NOCC}{$visuel}}) {
			print F "Archiving directory : $props{DESTDIR}/$prefix/$visuel into ${DESTARCHDIR}/$prefix/$visuel/\n";
			`mkdir -p ${DESTARCHDIR}/$prefix/$visuel/`;
			`mv $props{DESTDIR}/$prefix/$visuel/* ${DESTARCHDIR}/$prefix/$visuel/`;
				
				
            foreach my $id (sort keys %{$img{NOCC}{$visuel}{$type}}) {
                my $exist = 0;
                print F "File_NOCC : $img{NOCC}{$visuel}{$type}{$id}\n";
				
                if (-e "$props{DESTDIR}/$prefix/$visuel/$img{NOCC}{$visuel}{$type}{$id}" && $id == 1) {
                        $exist = 1;
                }
				`mkdir -p $props{DESTDIR}/$prefix/$visuel`;
				if ( (-s "$workingdir/$date/$img{NOCC}{$visuel}{$type}{$id}") > 4397152 ){
					`cp $workingdir/$date/$img{NOCC}{$visuel}{$type}{$id} $props{DESTDIR}/$prefix/$visuel/$img{NOCC}{$visuel}{$type}{$id}.orig`;
					`mv $workingdir/$date/$img{NOCC}{$visuel}{$type}{$id} $props{DESTDIR}/$prefix/$visuel/$img{NOCC}{$visuel}{$type}{$id}`;
					`jpegoptim --size=4000 $props{DESTDIR}/$prefix/$visuel/$img{NOCC}{$visuel}{$type}{$id}` ;
					print "Optimisation";
				} else {
					`mv $workingdir/$date/$img{NOCC}{$visuel}{$type}{$id} $props{DESTDIR}/$prefix/$visuel`;
				}
                
                #RWD
                resize("$props{DESTDIR}/$prefix/$visuel/$img{NOCC}{$visuel}{$type}{$id}","672","616","$props{DESTDIR}/$prefix/$visuel/G_".$visuel."_VFP_$id.jpg");
                resize("$props{DESTDIR}/$prefix/$visuel/$img{NOCC}{$visuel}{$type}{$id}","436","400","$props{DESTDIR}/$prefix/$visuel/G_".$visuel."_VPP_$id.jpg");
                resize("$props{DESTDIR}/$prefix/$visuel/$img{NOCC}{$visuel}{$type}{$id}","328","300","$props{DESTDIR}/$prefix/$visuel/G_".$visuel."_VPPM_$id.jpg");
                resize("$props{DESTDIR}/$prefix/$visuel/$img{NOCC}{$visuel}{$type}{$id}","66","60","$props{DESTDIR}/$prefix/$visuel/G_".$visuel."_VPMIN_$id.jpg");
                
                #RWD
                if ($id == 1 || $id == 2) {
                        resize("$props{DESTDIR}/$prefix/$visuel/$img{NOCC}{$visuel}{$type}{$id}","230","210","$props{DESTDIR}/$prefix/$visuel/G_".$visuel."_VPM_$id.jpg");
                        resize("$props{DESTDIR}/$prefix/$visuel/$img{NOCC}{$visuel}{$type}{$id}","120","110","$props{DESTDIR}/$prefix/$visuel/G_".$visuel."_VPC_$id.jpg");
                }
                
                if ($exist == 1 && $props{PURGE} == 1) {
                        print "Purge caches : ".$img{NOCC}{$visuel}{$type}{$id}."\n";
                        purge_VARNISH($visuel);
                        purge_CDN("/media/$prefix/$visuel/G_$visuel*");
                }
            }
        }

        #Webservice
#	print "Sync ecom : $visuel\n";
#        `curl -b /tmp/cookie.$hybrissrv.txt 'http://'$hybrissrv':9001/glwebservices/syncimages?pid='$visuel`;
        
	if (length($visuel) < 13) {
	    print "Sync rpu : $visuel\n";
	    `curl -b /tmp/cookierpu.$hybrissrv_rpu.txt 'http://'$hybrissrv_rpu':9001/glpcmwebservices/v1/mediasync?pid='$visuel`;
	}
}

sub treatment_CC {
        my $visuel = shift;
        my $prefix;
        $visuel =~ /(.{3})/;
        if ($visuel =~ /^3004/ && length($visuel) > 8) {
                $visuel =~ /(.{7})/;
                $prefix = $1;
        } else {
                $visuel =~ /(.{3})/;
                $prefix = $1;
        }
        foreach my $cc (keys %{$img{CC}{$visuel}}) {
				print F "Archiving directory : $props{DESTDIR}/$prefix/$visuel into ${DESTARCHDIR}/$prefix/$visuel/\p";
				`mkdir -p ${DESTARCHDIR}/$prefix/$visuel/`;
				`mv $props{DESTDIR}/$prefix/$visuel/* ${DESTARCHDIR}/$prefix/$visuel/`;
				
                foreach my $type (keys %{$img{CC}{$visuel}{$cc}}) {
                        foreach my $id (sort keys %{$img{CC}{$visuel}{$cc}{$type}}) {
                                my $exist = 0;
                                print F "File_CC : $img{CC}{$visuel}{$cc}{$type}{$id}\n";
								
								
                                if (-e "$props{DESTDIR}/$prefix/$visuel/$img{CC}{$visuel}{$cc}{$type}{$id}" && $id == 1) {
                                        $exist = 1;
                                }

						`mkdir -p $props{DESTDIR}/$prefix/$visuel`;
						if ( (-s "$workingdir/$date/$img{CC}{$visuel}{$cc}{$type}{$id}") > 4397152 ){
										`cp $workingdir/$date/$img{CC}{$visuel}{$cc}{$type}{$id} $props{DESTDIR}/$prefix/$visuel/$img{CC}{$visuel}{$cc}{$type}{$id}.orig`;
										`mv $workingdir/$date/$img{CC}{$visuel}{$cc}{$type}{$id} $props{DESTDIR}/$prefix/$visuel/$img{CC}{$visuel}{$cc}{$type}{$id}`;
							`jpegoptim -m70 --strip-none $props{DESTDIR}/$prefix/$visuel/$img{CC}{$visuel}{$cc}{$type}{$id}` ;
							print "Optimisation";
						} else {
										`mv $workingdir/$date/$img{CC}{$visuel}{$cc}{$type}{$id} $props{DESTDIR}/$prefix/$visuel`;
						}

                                #RWD
                                resize("$props{DESTDIR}/$prefix/$visuel/$img{CC}{$visuel}{$cc}{$type}{$id}","672","616","$props{DESTDIR}/$prefix/$visuel/G_".$visuel."_".$cc."_VFP_$id.jpg");
                                resize("$props{DESTDIR}/$prefix/$visuel/$img{CC}{$visuel}{$cc}{$type}{$id}","436","400","$props{DESTDIR}/$prefix/$visuel/G_".$visuel."_".$cc."_VPP_$id.jpg");
                                resize("$props{DESTDIR}/$prefix/$visuel/$img{CC}{$visuel}{$cc}{$type}{$id}","328","300","$props{DESTDIR}/$prefix/$visuel/G_".$visuel."_".$cc."_VPPM_$id.jpg");
                                resize("$props{DESTDIR}/$prefix/$visuel/$img{CC}{$visuel}{$cc}{$type}{$id}","66","60","$props{DESTDIR}/$prefix/$visuel/G_".$visuel."_".$cc."_VPMIN_$id.jpg");

                                #RWD
                                if ($id == 1 || $id == 2) {
                                        resize("$props{DESTDIR}/$prefix/$visuel/$img{CC}{$visuel}{$cc}{$type}{$id}","230","210","$props{DESTDIR}/$prefix/$visuel/G_".$visuel."_".$cc."_VPM_$id.jpg");
                                        resize("$props{DESTDIR}/$prefix/$visuel/$img{CC}{$visuel}{$cc}{$type}{$id}","120","110","$props{DESTDIR}/$prefix/$visuel/G_".$visuel."_".$cc."_VPC_$id.jpg");
                                }

                                if ($exist == 1 && $props{PURGE} == 1) {
                                        print "Purge caches : ".$img{CC}{$visuel}{$cc}{$type}{$id}."\n";
                                        purge_VARNISH($visuel);
                                        purge_CDN("/media/$prefix/$visuel/G_".$visuel."_$cc*");
                                }
                        }
                }
				if ( $full_cc eq "" ) {
                        $full_cc = $cc;
                } else {
                        $full_cc = $full_cc . '_' . $cc;
                }
        }
	$full_cc = $visuel . '_' . $full_cc;
	push(@ugprodCC,$full_cc);
}

sub treatment_P {
	my $visuel = shift;
	my $prefix;
    $visuel =~ /(.{3})/;
	if ($visuel =~ /^3004/ && length($visuel) > 8) {
        $visuel =~ /(.{7})/;
        $prefix = $1;
    } else {
        $visuel =~ /(.{3})/;
        $prefix = $1;
    }
	foreach my $cc (keys %{$img{P}{$visuel}}) {
		print F "Archiving directory : $props{DESTDIR}/$prefix/$visuel into ${DESTARCHDIR}/$prefix/$visuel/\p";
		`mkdir -p ${DESTARCHDIR}/$prefix/$visuel/`;
		`mv $props{DESTDIR}/$prefix/$visuel/* ${DESTARCHDIR}/$prefix/$visuel/`;
				
				
        foreach my $id (sort keys %{$img{P}{$visuel}{$cc}}) {
			my $exist = 0;
			print F "File_P : $img{P}{$visuel}{$cc}{$id}\n";
			
			my $final_dir = "$props{DESTDIR}/$prefix/$visuel";
			opendir (my $FhRep, $final_dir);
			my @Contenu = grep { !/^\.\.?$/ } readdir($FhRep);
			closedir ($FhRep);
			
			foreach my $FileFound (@Contenu) {
				if ( -f "$final_dir/$FileFound" && $FileFound =~ /P_${visuel}_${cc}_(\d+)\.(jpeg|jpg)$/) {
					delImg("$final_dir/$FileFound");
				}
			}
			
			if (-e "$props{DESTDIR}/$prefix/$visuel/$img{P}{$visuel}{$cc}{$id}") {
				$exist = 1;
				delImg("$props{DESTDIR}/$prefix/$visuel/$img{P}{$visuel}{$cc}{$id}");
			}
			
			`mkdir -p $props{DESTDIR}/$prefix/$visuel`;
			if ( (-s "$workingdir/$date/$img{P}{$visuel}{$cc}{$id}") > 4397152 ){
				`cp $workingdir/$date/$img{P}{$visuel}{$cc}{$id} $props{DESTDIR}/$prefix/$visuel/$img{P}{$visuel}{$cc}{$id}.orig`;
				`mv $workingdir/$date/$img{P}{$visuel}{$cc}${id} $props{DESTDIR}/$prefix/$visuel/$img{P}{$visuel}{$cc}{$id}`;
				`jpegoptim -m70 --strip-none $props{DESTDIR}/$prefix/$visuel/$img{P}{$visuel}{$cc}{$id}` ;
				print "Optimisation";	
			} else {
				`mv $workingdir/$date/$img{P}{$visuel}{$cc}{$id} $props{DESTDIR}/$prefix/$visuel/$img{P}{$visuel}{$cc}{$id}`;
			}
			if ($exist == 1 && $props{PURGE} == 1) {
				print "Purge caches : ".$img{$visuel}{$cc}{$id}."\n";
				purge_VARNISH($visuel);
				purge_CDN("/media/$prefix/$visuel/P_.$visuel*");
			}
		}
		if ( $full_cc eq "" ) {
                $full_cc = $cc;
        } else {
                $full_cc = $full_cc . '_' . $cc;
        }
	}
	$full_cc = $visuel . '_' . $full_cc;
	push(@ugprodCC,$full_cc);
}

sub getImg {
        `mkdir -p $workingdir/$date`;
        `scp $props{SRCSRV}:$props{SRCDIR}/* $workingdir/$date`;
        #archivage
	if ($props{SRCARCHDIR} ne 0) {
		print "archivage\n";
	        `ssh $props{SRCSRV} "mkdir -p $props{SRCARCHDIR}"`;
	        `ssh $props{SRCSRV} "mv -f $props{SRCDIR}/* $props{SRCARCHDIR}"`;
	}
}

sub delImg {
        unlink(<$_[0]*>);
}

#Start
checkParam();
loadProps();
checkLock();

#Start log
open(F, ">>$log") or die $!;
print F "$fulldate : Lancement de la procedure de resize des visuels\n";

#Set lock
open(F2, ">$lock".$props{LOCKSUFFIX}) or die $!;
print F2 "$$";
close F2,

#recup image
getImg();

#Construction liste fichier
opendir(D, "$workingdir/$date") or die $!;
while (my $filename = readdir(D)) {
	print $ft->checktype_filename("$workingdir/$date/$filename")."\n";
    if ($ft->checktype_filename("$workingdir/$date/$filename") =~ /jpeg/i && $filename =~ /G_(\d+)_((\d+)_([a-zA-Z]+)|([a-zA-Z]+))_(\d+)\.(jpeg|jpg)$/) {
		# Format 1: G_<UGProduit>_<CodeTeinte>_ZP_<Chiffre>.<format> ex: G_14509224_401_ZP_1.jpg
		# Format 2: G_300400849289_ZP_1.jpg G_3004<UGProduit>_ZP_<Chiffre>.<format>
		# Format 1 => $1 : UGProduit; $2 : CodeTeinte_ZP; $3 : CodeTeinte; $4 : ZP ;$5 : NA ; $6 : chiffre
		# Format 2 => $1 : 3004<UGProduit>; $2 : NA; $3 : NA; $4 : NA; $5 : ZP; $6 : chiffre
		if (($3 or $3 == 0) && $4) {
				$img{CC}{$1}{$3}{$4}{$6} = $filename;
		} else {
				$img{NOCC}{$1}{$5}{$6} = $filename;
		}
    } else {
		if ($ft->checktype_filename("$workingdir/$date/$filename") =~ /jpeg/i && $filename =~ /P_(\d+)_(\d+)_(\d+)\.(jpeg|jpg)$/) {
			my $ugprod = $1;
			my $cc = $2;
			my $id = $3;
			$img{P}{$ugprod}{$cc}{$id} = $filename;
		} else {
			unlink("$workingdir/$date/$filename") if ($filename !~ /^(\.|\.\.)$/);
		}
    }
}
closedir D;

#cookie webservice
print "Cr�ation cookie ecom \n";
`curl -d 'j_username=admin&j_password=uminda' -c /tmp/cookie.$hybrissrv.txt  'http://'$hybrissrv':9001/glwebservices/j_spring_security_check'`;
print "Cr�ation cookie rpu \n";
`curl -c /tmp/cookierpu.$hybrissrv_rpu.txt 'http://'$hybrissrv_rpu':9001/'`;

my $cpt = 0;
foreach my $visuel (keys %{$img{NOCC}}) {
        my $t = threads->new(\&treatment_noCC, $visuel);
        push(@threads, $t);
        $cpt++;

        if ($cpt > $maxth) {
                foreach (@threads) {
                        my $thrd = $_->join;
                }
                $cpt=0;
                undef(@threads);
        }
}

if ($#threads >= 0) {
        foreach (@threads) {
                my $thrd = $_->join;
        }
        undef(@threads);
}

$cpt = 0;
foreach my $visuel (keys %{$img{CC}}) {
        my $t = threads->new(\&treatment_CC, $visuel);
        push(@threads, $t);
		
        $cpt++;
        if ($cpt > $maxth) {
                foreach (@threads) {
                    $_->join();
                }
                $cpt=0;
                undef(@threads);
        }
}


if ($#threads >= 0) {
        foreach (@threads) {
                $_->join(); 
        }
        undef(@threads);
}

$cpt = 0;
foreach my $visuel (keys %{$img{P}}) {
	my $t = threads->new(\&treatment_P, $visuel);
	push(@threads, $t);
	
	$cpt++;
	if ($cpt > $maxth) {
		foreach (@threads) {
			$_->join();
		}
		$cpt=0;
		undef(@threads);
	}
}


if ($#threads >= 0) {
        foreach (@threads) {
                $_->join();
        }
        undef(@threads);
}

for (my $i = 0; $i <= scalar(@ugprodCC); $i++) {
	my @fields = split('_',$ugprodCC[$i],2);
	my $ugprod = $fields[0];
	$full_cc = $fields[1];
	my $add = "";
	for (my $j = 0; $j <= scalar(@ugprodCC); $j++) {
		if ( defined($ugprodCC[$j]) ) {
			my @fields2 = split('_',$ugprodCC[$j],2);
			my $ugprod_2 = $fields2[0];
			my $full_cc2 = $fields2[1];
			if ($ugprod == $ugprod_2) {
				if( $full_cc ne $full_cc2) {
					$add = $ugprod . '_' . $full_cc . '_' . $full_cc2;
				}
				else {
					$add = $ugprod . '_' . $full_cc;
				}
				if ($i ne $j){
					delete(@ugprodCC[$i]);
					delete(@ugprodCC[$j]);
				}
				else {
					delete(@ugprodCC[$i]);
				}
			}
		}
	}
	if ( $add != "" ) {
		push(@return,$add);
	}
}

foreach (@return) {
	my @fields = split('_',$_,2);
	my $ugprod = $fields[0];
#	print F "Sync ecom : $ugprod \n";
#	`curl -b /tmp/cookie.$hybrissrv.txt 'http://'$hybrissrv':9001/glwebservices/syncimages?pid='${ugprod}`;
	
	if (length($ugprod) < 13) {
		print F "Sync rpu : $ugprod\n";
		`curl -b /tmp/cookierpu.$hybrissrv_rpu.txt 'http://'$hybrissrv_rpu':9001/glpcmwebservices/v1/mediasync?pid='$_`;
	}
}


#Fin
unlink($lock.$props{LOCKSUFFIX});
print F "FIN\n";
close F;
