Select Distinct Parentp.Code As Ugproduit
  ,Ca.P_Id Catalog
  ,cv.P_VERSION Catalog_version
  ,CAT.Sous_Sous_Famille
  ,CAT.Code_Ssfam
  ,CAT.Ssfam_Catalog
  ,CAT.Ssfam_Catalog_version
  ,CAT.code_univers

From Gl_DEV.Products Parentp
left outer join Gl_DEV.productslp PLP on Parentp.PK = PLP.ITEMPK
left outer join (
        select UNIVCATLP.P_NAME as GROUPE
            -- , UNIVCATLP.P_CODE as CodeGROUPE
             , PARENTCATLP.P_NAME as CAT
             , FAMPRO.PK as PRO
             , SUPP.P_NAME as STATUT_FRN
             , PARENTCAT.P_CODE as CODE_CAT
             from Gl_DEV.PRODUCTS FAMPRO
        left join Gl_DEV.CAT2PRODREL C2P on C2P.TARGETPK = FAMPRO.PK
        left join Gl_DEV.CATEGORIES CAT on C2P.SOURCEPK = CAT.PK
        left join Gl_DEV.CATEGORIESLP CATLP on CATLP.itempk = CAT.PK
        left join Gl_DEV.COMPOSEDTYPES ITEMTYPE on ITEMTYPE.pk = CATLP.itemtypepk
        left join Gl_DEV.CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
        left join Gl_DEV.CATEGORIESLP PARENTCATLP on PARENTCATLP.ITEMPK = C2C.SOURCEPK
        left join Gl_DEV.CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
        left join Gl_DEV.ENUMERATIONVALUESLP SUPP on SUPP.ITEMPK = PARENTCAT.P_SUPPLIERSTATUS
        left join Gl_DEV.CAT2CATREL TOPC2C on TOPC2C.TARGETPK = PARENTCATLP.ITEMPK
        left join Gl_DEV.CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
        left join Gl_DEV.CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
        left join Gl_DEV.CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
        where ITEMTYPE.P_SHORTID='RSF'
        ) RMS on RMS.PRO = PARENTP.P_BASEPRODUCT
left outer join (
        select UNIVCATLP.P_NAME UNIVERS
             , UNIVCAT.P_CODE code_univers
             , PARENTCATLP.P_NAME Sous_famille
             , PARENTCAT.P_CODE code_sfam
             , TOPCAT.P_CODE code_fam
             , TOPCATLP.P_NAME Famille
             , CAT.P_CODE code_ssfam
             , CATLP.P_NAME Sous_sous_famille
             , Fampro.Pk Pro
             , ca.P_Id ssfam_catalog
             , cv.P_VERSION ssfam_catalog_version
             from Gl_DEV.PRODUCTS FAMPRO
                left join Gl_DEV.CAT2PRODREL C2P on C2P.TARGETPK = FAMPRO.PK
                left join Gl_DEV.CATEGORIES CAT on C2P.SOURCEPK = CAT.PK
                left join Gl_DEV.CATEGORIESLP CATLP on CATLP.itempk = CAT.PK
                left join Gl_DEV.COMPOSEDTYPES ITEMTYPE on ITEMTYPE.pk = CATLP.itemtypepk
                left join Gl_DEV.CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
                left join Gl_DEV.CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
                left join Gl_DEV.CATEGORIESLP PARENTCATLP on PARENTCATLP.ITEMPK = C2C.SOURCEPK
                left join Gl_DEV.CAT2CATREL TOPC2C on TOPC2C.TARGETPK = PARENTCATLP.ITEMPK
                left join Gl_DEV.CATEGORIES TOPCAT on TOPCAT.PK = TOPC2C.SOURCEPK
                left join Gl_DEV.CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
                left join Gl_DEV.CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
                left join Gl_DEV.CATEGORIES UNIVCAT on UNIVCAT.PK = UNIVC2C.SOURCEPK
                left join Gl_DEV.CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
        left join Gl_DEV.Catalogs ca ON CAT.P_Catalog = ca.Pk
        left join Gl_DEV.CatalogVersions cv ON CAT.p_catalogVersion = cv.pk
        where ITEMTYPE.P_SHORTID='PSSF' AND CAT.P_CODE not like '%CLA%' AND PARENTCAT.P_CODE not like '%CLA%'
        ) Cat On Cat.Pro = Parentp.Pk
    left join Gl_DEV.Catalogs ca ON Parentp.P_Catalog = ca.Pk
    left join Gl_DEV.CatalogVersions cv ON Parentp.p_catalogVersion = cv.pk
Where PARENTP.P_BASEPRODUCT is null AND  PARENTP.p_catalogversion=8796093121113 and CAT.code_univers='PU_1';




        select UNIVCATLP.P_NAME UNIVERS
             , UNIVCAT.P_CODE code_univers
            , TOPCAT.P_CODE code_fam
             , TOPCATLP.P_NAME Famille
             , PARENTCATLP.P_NAME Sous_famille
             , PARENTCAT.P_CODE code_sfam
             , CAT.P_CODE code_ssfam
             , CATLP.P_NAME Sous_sous_famille
             , Fampro.Pk Pro
             , cv.P_VERSION ssfam_catalog_version
             ,FAMPRO.P_CATALOGVERSION
             from Gl_DEV.PRODUCTS FAMPRO
                left join Gl_DEV.CAT2PRODREL C2P on C2P.TARGETPK = FAMPRO.PK
                left join Gl_DEV.CATEGORIES CAT on C2P.SOURCEPK = CAT.PK
                left join Gl_DEV.CATEGORIESLP CATLP on CATLP.itempk = CAT.PK
                left join Gl_DEV.COMPOSEDTYPES ITEMTYPE on ITEMTYPE.pk = CATLP.itemtypepk
                left join Gl_DEV.CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
                left join Gl_DEV.CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
                left join Gl_DEV.CATEGORIESLP PARENTCATLP on PARENTCATLP.ITEMPK = C2C.SOURCEPK
                left join Gl_DEV.CAT2CATREL TOPC2C on TOPC2C.TARGETPK = PARENTCATLP.ITEMPK
                left join Gl_DEV.CATEGORIES TOPCAT on TOPCAT.PK = TOPC2C.SOURCEPK
                left join Gl_DEV.CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
                left join Gl_DEV.CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
                left join Gl_DEV.CATEGORIES UNIVCAT on UNIVCAT.PK = UNIVC2C.SOURCEPK
                left join Gl_DEV.CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
        left join Gl_DEV.Catalogs ca ON CAT.P_Catalog = ca.Pk
        left join Gl_DEV.CatalogVersions cv ON CAT.p_catalogVersion = cv.pk
        where ITEMTYPE.P_SHORTID='PSSF' AND CAT.P_CODE not like '%CLA%' AND PARENTCAT.P_CODE not like '%CLA%'
        and UNIVCATLP.P_NAME in ('Femme','Homme') and FAMPRO.P_CATALOGVERSION=8796093121113;






