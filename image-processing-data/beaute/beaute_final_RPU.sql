select rmsa.p_ug as ug_article, rmsp.p_ug as ug_product
from RMSARTICLES rmsa
left join RMSPRODUCTS rmsp on rmsa.p_product = rmsp.pk
left join OFFERS o on o.p_code = rmsa.p_ug
left join PRODUCTS article on article.pk = o.p_article
left join PRODUCTS product on product.pk = article.P_BASEPRODUCT
left join CAT2PRODREL C2P on C2P.TARGETPK = product.PK
left join CATEGORIES CAT on C2P.SOURCEPK = CAT.PK
left join CATEGORIESLP CATLP on CATLP.itempk = CAT.PK
left join COMPOSEDTYPES ITEMTYPE on ITEMTYPE.pk = CATLP.itemtypepk
left join CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
left join CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
left join CAT2CATREL TOPC2C on TOPC2C.TARGETPK = C2C.SOURCEPK
left join CATEGORIES TOPCAT on TOPCAT.PK = TOPC2C.SOURCEPK
left join CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
left join CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
left join CATEGORIES UNIVCAT on UNIVCAT.PK = UNIVC2C.SOURCEPK
left join CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
where UNIVCAT.P_CODE not like 'CL%'and CAT.P_CODE not like '%CLA%' AND PARENTCAT.P_CODE not like '%CLA%'
and UNIVCAT.P_CODE = 'PU_1'
; 

select * from ENRICHWFCTX ewc
left join products article on article.pk = ewc.P_ARTICLEFORARTICLEDATAEXPORT
left join OFFERS o on o.p_article = article.pk

left join RMSARTICLES rmsa on rmsa.p_ug = o.P_CODE
; 