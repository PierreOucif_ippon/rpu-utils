#!/bin/bash

for line in $1* 
do
   N0=`echo $line | cut -d'/' -f2`
   N1=`echo $N0 | cut -d'_' -f1`
   N2=`echo $N0 | cut -d'_' -f2`
   N3=`echo $N0 | cut -d'_' -f3`
   N4=`echo $N0 | cut -d'_' -f4`
   IMAGE="${N1}_${N2}_${N3}_ZP_${N4}"
   echo "${line}"
   echo "${2}${IMAGE}"
   #cp ${1}${line} ${2}${IMAGE}
done
