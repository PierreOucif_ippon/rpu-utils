select distinct rmsp.p_ug as ug_product, rmsaexport.p_ug as ug_article from ENRICHWFCTX ewc
left join ENRICHWFCTX2ART rel on rel.SOURCEPK=ewc.pk
left join PRODUCTS article on article.pk = rel.TARGETPK
left join PRODUCTS aexport on aexport.pk = ewc.P_ARTICLEFORARTICLEDATAEXPORT
left join OFFERS oexport on oexport.p_article = aexport.pk
left join rmsarticles rmsaexport on rmsaexport.p_ug = oexport.p_code
left join OFFERS o on o.P_ARTICLE = article.pk
left join RMSARTICLES rmsa on rmsa.p_ug = o.p_code
left join RMSPRODUCTS rmsp on rmsp.pk = rmsa.p_product
left join PRODUCTS product on product.pk = article.P_BASEPRODUCT
left join CAT2PRODREL C2P on C2P.TARGETPK = product.PK
left join CATEGORIES CAT on C2P.SOURCEPK = CAT.PK
left join CATEGORIESLP CATLP on CATLP.itempk = CAT.PK
left join COMPOSEDTYPES ITEMTYPE on ITEMTYPE.pk = CATLP.itemtypepk
left join CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
left join CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
left join CAT2CATREL TOPC2C on TOPC2C.TARGETPK = C2C.SOURCEPK
left join CATEGORIES TOPCAT on TOPCAT.PK = TOPC2C.SOURCEPK
left join CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
left join CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
left join CATEGORIES UNIVCAT on UNIVCAT.PK = UNIVC2C.SOURCEPK
left join CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
left join CAT2PRODREL C2P2 on C2P2.TARGETPK = product.PK
left join CATEGORIES CAT2 on C2P2.SOURCEPK = CAT2.PK
left join CATEGORIESLP CATLP2 on CATLP2.itempk = CAT2.PK
left join CAT2CATREL C2C2 on C2C2.TARGETPK = CAT2.PK
left join CATEGORIES PARENTCAT2 on PARENTCAT2.PK = C2C2.SOURCEPK
left join CATEGORIESLP PARENTCATLP2 on PARENTCATLP2.ITEMPK = C2C2.SOURCEPK
left join CAT2CATREL TOPC2C2 on TOPC2C2.TARGETPK = PARENTCATLP2.ITEMPK
left join CATEGORIESLP TOPCATLP2 on TOPCATLP2.ITEMPK = TOPC2C2.SOURCEPK
left join CAT2CATREL UNIVC2C2 on UNIVC2C2.TARGETPK = TOPCATLP2.ITEMPK
left join CATEGORIES UNIVCAT2 on UNIVCAT2.PK = UNIVC2C2.SOURCEPK
left join productfeatures pf2 on pf2.p_product = article.pk
left join CLASSATTRVALUES cav2 on cav2.pk = cast(pf2.P_STRINGVALUE as varchar2(3999))
left join classvarianttypes cvt2 on cvt2.pk = product.P_CLASSIFICATIONVARIANTTYPE
left join classificationattrs ca2 on ca2.pk = cvt2.P_MAINCLASSIFICATIONATTRIBUTE
left join productfeatures pff2 on pff2.p_product = article.pk
left join CLASSATTRVALUES cavv2 on cavv2.pk = cast(pff2.P_STRINGVALUE as varchar2(3999))
where UNIVCAT2.P_CODE not like 'CL%' and CAT2.P_CODE not like '%CLA%' AND PARENTCAT2.P_CODE not like '%CLA%'
and cav2.p_code like concat(ca2.p_code,'%')
and cavv2.p_code like ('SIZE%')
and UNIVCAT.P_CODE not like 'CL%'and CAT.P_CODE not like '%CLA%' AND PARENTCAT.P_CODE not like '%CLA%'
and UNIVCAT.P_CODE = 'PU_1'
;


select rmsa.p_ug
             , cav2.P_CODE code_main_classif
from RMSARTICLES rmsa
left join offers oo on oo.P_CODE = rmsa.P_UG
left join PRODUCTS articlee on oo.P_ARTICLE = articlee.pk
left join PRODUCTS productt on productt.pk = articlee.P_BASEPRODUCT
left join CAT2PRODREL C2P2 on C2P2.TARGETPK = productt.PK
left join CATEGORIES CAT2 on C2P2.SOURCEPK = CAT2.PK
left join CATEGORIESLP CATLP2 on CATLP2.itempk = CAT2.PK
left join CAT2CATREL C2C2 on C2C2.TARGETPK = CAT2.PK
left join CATEGORIES PARENTCAT2 on PARENTCAT2.PK = C2C2.SOURCEPK
left join CATEGORIESLP PARENTCATLP2 on PARENTCATLP2.ITEMPK = C2C2.SOURCEPK
left join CAT2CATREL TOPC2C2 on TOPC2C2.TARGETPK = PARENTCATLP2.ITEMPK
left join CATEGORIESLP TOPCATLP2 on TOPCATLP2.ITEMPK = TOPC2C2.SOURCEPK
left join CAT2CATREL UNIVC2C2 on UNIVC2C2.TARGETPK = TOPCATLP2.ITEMPK
left join CATEGORIES UNIVCAT2 on UNIVCAT2.PK = UNIVC2C2.SOURCEPK
left join productfeatures pf2 on pf2.p_product = articlee.pk
left join CLASSATTRVALUES cav2 on cav2.pk = cast(pf2.P_STRINGVALUE as varchar2(3999))
left join classvarianttypes cvt2 on cvt2.pk = productt.P_CLASSIFICATIONVARIANTTYPE
left join classificationattrs ca2 on ca2.pk = cvt2.P_MAINCLASSIFICATIONATTRIBUTE
left join productfeatures pff2 on pff2.p_product = articlee.pk
left join CLASSATTRVALUES cavv2 on cavv2.pk = cast(pff2.P_STRINGVALUE as varchar2(3999))
where UNIVCAT2.P_CODE not like 'CL%' and CAT2.P_CODE not like '%CLA%' AND PARENTCAT2.P_CODE not like '%CLA%'
and cav2.p_code like concat(ca2.p_code,'%')
and cavv2.p_code like ('SIZE%')
and ROWNUM = 1;