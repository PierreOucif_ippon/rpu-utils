#!/usr/bin/perl

use strict;

use DBI;
use DBD::Oracle;
use Switch;

sub connectHybris {
        my ($bdd_server , $port, $SID, $login, $passwd) = @_;
        return DBI->connect( "dbi:Oracle:host=$bdd_server;sid=$SID;port=$port", $login, $passwd ) || die( $DBI::errstr . "\n" );
}

sub get_sql_from_file {
    open my $fh, '<', shift or die "Can't open SQL File for reading: $!";
    local $/;
    return <$fh>;
};

my @ecom_credentials=qw(jupiter 1521 GLI gl_dev gli_gl_dev);
my @rpu_credentials=qw(localhost 1521 GLB2 GL_HYBRIS_2 GL_HYBRIS_22);



#if ( ! -f "./impex-builder-".$IMPEX_BUILDER_VERSION."-jar-with-dependencies.jar" ){
#print "Download impex-builder-".$IMPEX_BUILDER_VERSION."-jar-with-dependencies.jar\n";
#system( "wget --user=reader --password=AP7eFPdUzjKQB4hTfwQBZmeAMLU -q http://soleil.int-gl.com/artifactory/releases-local/com/galerieslafayette/ecom/impex-builder/".$IMPEX_BUILDER_VERSION."/impex-builder-".$IMPEX_BUILDER_VERSION."-jar-with-dependencies.jar")  == 0
#	 or die "Download impex-builder-".$IMPEX_BUILDER_VERSION."-jar-with-dependencies.jar failed: $?";
#}

my $sql_file;
my $dbh;
$ENV{NLS_LANG} = 'FRENCH_FRANCE.UTF8';

my $hasHead=0;

	$dbh= connectHybris(@rpu_credentials);
	$sql_file="RPU_Requete_Moulinette.sql"


	my $SQL = get_sql_from_file($sql_file);
	$SQL =~ s/\);/)/;

	#$dbh->{LongReadLen} = 81920;
	my $req=$dbh->prepare($SQL);
	$req->execute;

	print "generate data.csv\n";

	open(DATA_FILE, '>:utf8', "data.csv") or die "cannot open file";

	$hasHead=0;

	#write column data
	while (my @row = $req->fetchrow_array) {
		if ( $hasHead == 0 ){
			#write column name
			print DATA_FILE join( "|", @{$req->{NAME}})."\n";
			$hasHead=1;
		}
		print DATA_FILE join( "|",@row )."\n";
	}

	close(DATA_FILE);


