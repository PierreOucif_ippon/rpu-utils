#!/bin/bash

rep1=oucif/
rep2=tmp/

ls -lrt $rep1 >> ./ls_rep1.txt
ls -lrt $rep2 >> ./ls_rep2.txt
COMP="KO"
cat ./ls_rep1.txt | while read line; do
                name_1=`echo $line | cut -d' ' -f9`
                cat ./ls_rep2.txt | while read line; do
                               if [ "$COMP" == "KO" ]; then
                                               name_2=`echo $line | cut -d' ' -f9`
                                               
                                               if [ "$name_1" == "$name_2" ];then
                                                               COMP="OK"
                                               fi
                               fi
                done
                if [ "$COMP" == "KO" ]; then
                               echo "$name_1 present uniquement dans $rep1" >> ./result_diff.txt
                fi
done
