#!/bin/bash

find $1 -name "*.jpg" | while read line; do
  DESTREP=`echo $line | cut -d'/' -f2`
  IMAGE_NAME=`echo $line | cut -d'/' -f6`
  N1=`echo $IMAGE_NAME | cut -d'_' -f1`
  N2=`echo $IMAGE_NAME | cut -d'_' -f2`
  N3=`echo $IMAGE_NAME | cut -d'_' -f3`
  N4=`echo $IMAGE_NAME | cut -d'_' -f4`
  if [ "$N4" != "ZP" ]; then
          F=${1}/${N1}_${N2}_${N3}_ZP_${N4}
        echo "mv $line ./${DESTREP}${F}"
          mv $line ./${DESTREP}${F}
  fi
done

