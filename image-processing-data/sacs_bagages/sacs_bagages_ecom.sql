
        select DISTINCT FAMPRO.CODE
             from PRODUCTS FAMPRO
                left join CAT2PRODREL C2P on C2P.TARGETPK = FAMPRO.PK
                left join CATEGORIES CAT on C2P.SOURCEPK = CAT.PK
                left join CATEGORIESLP CATLP on CATLP.itempk = CAT.PK
                left join COMPOSEDTYPES ITEMTYPE on ITEMTYPE.pk = CATLP.itemtypepk
                left join CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
                left join CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
                left join CATEGORIESLP PARENTCATLP on PARENTCATLP.ITEMPK = C2C.SOURCEPK
                left join CAT2CATREL TOPC2C on TOPC2C.TARGETPK = PARENTCATLP.ITEMPK
                left join CATEGORIES TOPCAT on TOPCAT.PK = TOPC2C.SOURCEPK
                left join CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
                left join CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
                left join CATEGORIES UNIVCAT on UNIVCAT.PK = UNIVC2C.SOURCEPK
                left join CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
        left join Catalogs ca ON CAT.P_Catalog = ca.Pk
        left join CatalogVersions cv ON CAT.p_catalogVersion = cv.pk
        where ITEMTYPE.P_SHORTID='PSSF' AND CAT.P_CODE not like '%CLA%' AND PARENTCAT.P_CODE not like '%CLA%'
        and UNIVCATLP.P_NAME in ('Homme','Femme','Fille','Mixte') and TOPCATLP.P_NAME = 'Sacs et bagages'
        and FAMPRO.P_CATALOGVERSION=8796093121113;