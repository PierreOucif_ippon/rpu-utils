SELECT distinct ewc.P_SUPPLIERIMAGES  FROM products product 
JOIN enrichwfctx2art item_t1 ON  product.PK = item_t1.TargetPK  
JOIN enrichwfctx ewc ON  ewc.PK = item_t1.SourcePK
where product.P_EAN in (
select distinct rmsa.P_EAN
from GL_RPU.RMSARTICLES rmsa
        left join GL_RPU.RMSPRODUCTS rmsp on rmsp.pk=rmsa.P_PRODUCT
        left join GL_RPU.OFFERS o on O.P_CODE=Rmsa.P_UG
        left join GL_RPU.products art on o.p_article=art.pk
        left join GL_RPU.products pro on art.P_BASEPRODUCT=pro.pk
        left join GL_RPU.productslp plp on art.pk=plp.ITEMPK
        left join GL_RPU.ENUMERATIONVALUESLP Saison on Saison.itempk=rmsa.p_season
        left join GL_RPU.ENUMERATIONVALUES Sea on Sea.pk=rmsa.p_season
        left join GL_RPU.CATEGORIES CAT on rmsp.P_SUBFAMILY=CAT.PK
        left join GL_RPU.CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
        left join GL_RPU.CATEGORIESLP PARENTCATLP on PARENTCATLP.ITEMPK = C2C.SOURCEPK
        left join GL_RPU.CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
        left join GL_RPU.ENUMERATIONVALUESLP SUPP on SUPP.ITEMPK = PARENTCAT.P_SUPPLIERSTATUS
        left join GL_RPU.ENUMERATIONVALUESLP MNMP on MNMP.ITEMPK = PARENTCAT.P_SUPPLIERSUBGROUP
       left join GL_RPU.CAT2CATREL TOPC2C on TOPC2C.TARGETPK = PARENTCATLP.ITEMPK
        left join GL_RPU.CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
        left join GL_RPU.CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
        left join GL_RPU.CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
where UNIVCATLP.P_NAME = 'Parfumerie/Beaut�');


SELECT * FROM enrichwfctx ewc 
JOIN enrichwfctx2art rela ON  rela.SourcePK = ewc.PK  
JOIN products pp ON  pp.PK = rela.TargetPK
WHERE pp.PK in (
select * from GL_RPU.products product
where product.P_EAN in (
select distinct rmsa.P_EAN
from GL_RPU.RMSARTICLES rmsa
        left join GL_RPU.RMSPRODUCTS rmsp on rmsp.pk=rmsa.P_PRODUCT
        left join GL_RPU.OFFERS o on O.P_CODE=Rmsa.P_UG
        left join GL_RPU.products art on o.p_article=art.pk
        left join GL_RPU.products pro on art.P_BASEPRODUCT=pro.pk
        left join GL_RPU.productslp plp on art.pk=plp.ITEMPK
        left join GL_RPU.ENUMERATIONVALUESLP Saison on Saison.itempk=rmsa.p_season
        left join GL_RPU.ENUMERATIONVALUES Sea on Sea.pk=rmsa.p_season
        left join GL_RPU.CATEGORIES CAT on rmsp.P_SUBFAMILY=CAT.PK
        left join GL_RPU.CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
        left join GL_RPU.CATEGORIESLP PARENTCATLP on PARENTCATLP.ITEMPK = C2C.SOURCEPK
        left join GL_RPU.CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
        left join GL_RPU.ENUMERATIONVALUESLP SUPP on SUPP.ITEMPK = PARENTCAT.P_SUPPLIERSTATUS
        left join GL_RPU.ENUMERATIONVALUESLP MNMP on MNMP.ITEMPK = PARENTCAT.P_SUPPLIERSUBGROUP
       left join GL_RPU.CAT2CATREL TOPC2C on TOPC2C.TARGETPK = PARENTCATLP.ITEMPK
        left join GL_RPU.CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
        left join GL_RPU.CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
        left join GL_RPU.CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
where UNIVCATLP.P_NAME = 'Parfumerie/Beaut�')
);

select * from GL_RPU.product
where GL_RPU.product.P_EAN in (
select distinct rmsa.P_EAN
from GL_RPU.RMSARTICLES rmsa
        left join GL_RPU.RMSPRODUCTS rmsp on rmsp.pk=rmsa.P_PRODUCT
        left join GL_RPU.OFFERS o on O.P_CODE=Rmsa.P_UG
        left join GL_RPU.products art on o.p_article=art.pk
        left join GL_RPU.products pro on art.P_BASEPRODUCT=pro.pk
        left join GL_RPU.productslp plp on art.pk=plp.ITEMPK
        left join GL_RPU.ENUMERATIONVALUESLP Saison on Saison.itempk=rmsa.p_season
        left join GL_RPU.ENUMERATIONVALUES Sea on Sea.pk=rmsa.p_season
        left join GL_RPU.CATEGORIES CAT on rmsp.P_SUBFAMILY=CAT.PK
        left join GL_RPU.CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
        left join GL_RPU.CATEGORIESLP PARENTCATLP on PARENTCATLP.ITEMPK = C2C.SOURCEPK
        left join GL_RPU.CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
        left join GL_RPU.ENUMERATIONVALUESLP SUPP on SUPP.ITEMPK = PARENTCAT.P_SUPPLIERSTATUS
        left join GL_RPU.ENUMERATIONVALUESLP MNMP on MNMP.ITEMPK = PARENTCAT.P_SUPPLIERSUBGROUP
       left join GL_RPU.CAT2CATREL TOPC2C on TOPC2C.TARGETPK = PARENTCATLP.ITEMPK
        left join GL_RPU.CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
        left join GL_RPU.CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
        left join GL_RPU.CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
where UNIVCATLP.P_NAME = 'Parfumerie/Beaut�');