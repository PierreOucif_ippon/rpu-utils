$systemName=pcm_catalog
$systemVersion=Staged
$sysVer=catalogVersion(catalog(id[default=$systemName]), version[default=$systemVersion])[unique=true]
$lang=fr 

UPDATE PcmSubSubFamily;code[unique=true];name[lang=$lang];$sysVer
;PSSF245;Lunettes de soleil
;PSSF752;Lunettes de soleil
;PSSF448;Lunettes de soleil