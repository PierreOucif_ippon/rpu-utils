select 
  P_UG as P_UG
  , manif
  , process
  , to_char(Deversement,'dd/mm/yyyy') as Deversement
  , to_char(Transco_En_Cours,'dd/mm/yyyy') as Transco_En_Cours
  , to_char(Sans_famille_RMS,'dd/mm/yyyy') as Sans_famille_RMS
  , to_char(Choix_Process,'dd/mm/yyyy') as Choix_Process
  , to_char(Attente_Stock,'dd/mm/yyyy') as Attente_Stock
  , to_char(Eligible_Picking,'dd/mm/yyyy') as Eligible_Picking
  , to_char(Hors_Workflow,'dd/mm/yyyy') as Hors_Workflow
  , to_char(A_Enrichir,'dd/mm/yyyy') as A_Enrichir
  , to_char(Attente_Visuel,'dd/mm/yyyy') as Attente_Visuel
  , to_char(A_valider,'dd/mm/yyyy') as A_valider
  , to_char(Corriger_Fiche_produit,'dd/mm/yyyy') as Corriger_Fiche_produit
  , to_char(Trancode,'dd/mm/yyyy') as Trancode
  , to_char(Publie,'dd/mm/yyyy') as Publie
  , Jalon



FROM (

select 
  P_UG as P_UG
  , max(manif) as manif
  , min(PROCESS) as process
  , max(Deversement) as Deversement
  , max(Transco_En_Cours) as Transco_En_Cours
  , max(Sans_famille_RMS) as Sans_famille_RMS
  , max(Choix_Process) as Choix_Process
  , max(Attente_Stock) as Attente_Stock
  , max(Eligible_Picking) as Eligible_Picking
  , max(Hors_Workflow) as Hors_Workflow
  , max(A_Enrichir) as A_Enrichir
  , max(Attente_Visuel) as Attente_Visuel
  , max(A_valider) as A_valider
  , max(Corriger_Fiche_produit) as Corriger_Fiche_produit
  , max(Trancode) as Trancode
  , max(Publie) as Publie
  , max(GL_RPU.GET_WORKFLOW_STATUS_2(Deversement,Transco_En_Cours,Sans_famille_RMS,Choix_Process,Attente_Stock,Eligible_Picking,Hors_Workflow,A_Enrichir,Attente_Visuel,A_valider,Corriger_Fiche_produit,PUBLIE)) as Jalon
  
FROM (
  
select 
  P_UG
  ,manif
  , PROCESS 
  , DATEDEVSERVEMENT as Deversement
  , TRANSCO_EN_COURS as Transco_En_Cours
  , SANS_FAMILLE_RMS as Sans_famille_RMS
  , CHOIX_PROCESS as Choix_Process
  , ATTENTE_STOCK as Attente_Stock
  , ELIGIBLE_PICKING as Eligible_Picking
  , HORS_WORKFLOW as Hors_Workflow
  , A_ENRICHIR as A_Enrichir
  , ATTENTE_VISUEL as Attente_Visuel
  , A_VALIDER as A_valider
  , CORRECTION_FICHE_PRODUIT as Corriger_Fiche_produit
  , TRANCODE as Trancode
  , decode(TRANCODE,NULL,PUBLIE,TRANCODE) as Publie
    
FROM (

select * 

 from (

select R.createdts as dateDevservement, /*'Enrichissement' as Workflow,*/ R.p_ug, Actlp.p_name,
case 
  when Actlp.p_name='Enrichissement de donn�es' then Actlink.modifiedts
  else Act.p_firstactivated
END as DATE_DECISION,
EClp.P_NAME as Process, manif.p_name as Manif
from GL_RPU.WORKFLOWITEMATTS Att
left join GL_RPU.ENRICHWFCTX ECtx ON Att.p_item=ECtx.pk
left join GL_RPU.workflowactions Act ON Att.P_WORKFLOW=Act.P_WORKFLOW
left join GL_RPU.workflowactionslp Actlp ON Act.pk=Actlp.itempk
LEFT JOIN GL_RPU.workflowactionlinkrel Actlink ON Act.P_SELECTEDDECISION=Actlink.sourcepk
left join GL_RPU.enumerationvalues enum1 ON Act.P_Status=Act.pk
left join GL_RPU.enumerationvalueslp enumlp1 ON enum1.pk=enumlp1.itempk
left join GL_RPU.ENRICHWFCONFLP EClp on ECtx.P_ENRICHMENTWORKFLOWCONFIGURAT=EClp.itemPK
left join GL_RPU.ENRICHWFCTX2ART Rel ON Rel.sourcepk=ECtx.pk
left join GL_RPU.PRODUCTS PR on Rel.targetpk=PR.pk
left join GL_RPU.OFFERS O ON PR.pk=O.p_article
LEFT JOIN GL_RPU.MANIFCODESLP manif ON O.P_MANIFCODE = manif.itempk 
left join GL_RPU.RMSARTICLES R on O.P_CODE=R.P_UG
left join GL_RPU.PICKINGS P on P.P_RMSARTICLE=R.PK
left join GL_RPU.cronjobslp clp ON substr(clp.p_name,30,8)=R.P_UG
left join GL_RPU.cronjobs cr ON clp.itempk=cr.pk
left join GL_RPU.enumerationvalues enum2 on R.P_MATCHINGSTATUS=enum2.pk
LEFT JOIN GL_RPU.ENUMERATIONVALUES sea ON R.P_SEASON = sea.pk
LEFT JOIN GL_RPU.ENUMERATIONVALUESLP sealp ON R.P_SEASON = sealp.itempk 
WHERE  enum2.CODE <>'MATCHING_VALIDATED'
AND Sea.code in ('115','315','515','316','516','116')
--AND R.p_ug in (33676937,34515839) 

UNION

select R.createdts as dateDevservement, /*'Transco' as Workflow,*/ R.p_ug, 'Transco Validee' as p_name, cr.P_ENDTIME as p_firstactivated, 'Transco' as Process, manif.p_name as Manif
from GL_RPU.cronjobslp clp
left join GL_RPU.RMSARTICLES R ON substr(clp.p_name,30,8)=R.P_UG
left join GL_RPU.cronjobs cr ON clp.itempk=cr.pk
left join GL_RPU.enumerationvalues enum on R.P_MATCHINGSTATUS=enum.pk
LEFT JOIN GL_RPU.ENUMERATIONVALUES sea ON R.P_SEASON = sea.pk
LEFT JOIN GL_RPU.ENUMERATIONVALUESLP sealp ON R.P_SEASON = sealp.itempk
left join GL_RPU.OFFERS O ON R.P_UG=O.p_article
LEFT JOIN GL_RPU.MANIFCODESLP manif ON O.P_MANIFCODE = manif.itempk 
WHERE enum.CODE='MATCHING_VALIDATED'
--AND substr(clp.p_name,30,8) in ('33676937','34515839')
AND Sea.code in ('115','315','515','316','516','116')

UNION

select R.createdts as dateDevservement, /*'Pilotage' as Workflow,*/ R.p_ug, Actlp.p_name, Act.p_firstactivated, EClp.P_NAME as Process, manif.p_name as Manif
from GL_RPU.WORKFLOWITEMATTS Att
left join GL_RPU.STEERINGWFCTX SCtx ON Att.p_item=SCtx.pk
left join GL_RPU.workflowactions Act ON Att.P_WORKFLOW=Act.P_WORKFLOW
left join GL_RPU.workflowactionslp Actlp ON Act.pk=Actlp.itempk
left join GL_RPU.enumerationvalues enum ON Act.P_Status=Act.pk
left join GL_RPU.enumerationvalueslp enumlp ON enum.pk=enumlp.itempk
left join GL_RPU.ENRICHWFCONFLP EClp on SCtx.P_ENRICHMENTWORKFLOWCONFIGURAT=EClp.itemPK
left join GL_RPU.STRGWFCTX2RMSARTS Rel ON Rel.sourcepk=SCtx.pk
left join GL_RPU.RMSARTICLES R on Rel.targetpk=R.pk
left join GL_RPU.PICKINGS P on P.P_RMSARTICLE=R.PK
left join GL_RPU.cronjobslp clp ON substr(clp.p_name,30,8)=R.P_UG
left join GL_RPU.cronjobs cr ON clp.itempk=cr.pk
left join GL_RPU.enumerationvalues enum2 on R.P_MATCHINGSTATUS=enum2.pk
LEFT JOIN GL_RPU.ENUMERATIONVALUESLP sealp ON R.P_SEASON = sealp.itempk
LEFT JOIN GL_RPU.ENUMERATIONVALUES sea ON R.P_SEASON = sea.pk
left join GL_RPU.OFFERS O on O.P_CODE=R.P_UG
LEFT JOIN GL_RPU.MANIFCODESLP manif ON O.P_MANIFCODE = manif.itempk 

WHERE --R.p_ug in ('33676937','34515839') AND
  Sea.code in ('115','315','515','316','516','116')

UNION

select R.createdts as dateDevservement, /*'Transco' as Workflow,*/ R.p_ug, 'Transco en cours' as p_name, (R.createdts + .00001157407) as p_firstactivated, 'Transco' as Process, manif.p_name as Manif
from GL_RPU.cronjobslp clp
left join GL_RPU.RMSARTICLES R ON substr(clp.p_name,30,8)=R.P_UG
left join GL_RPU.cronjobs cr ON clp.itempk=cr.pk
left join GL_RPU.enumerationvalues enum on R.P_MATCHINGSTATUS=enum.pk
LEFT JOIN GL_RPU.ENUMERATIONVALUESLP sealp ON R.P_SEASON = sealp.itempk
LEFT JOIN GL_RPU.ENUMERATIONVALUES sea ON R.P_SEASON = sea.pk
left join GL_RPU.OFFERS O ON R.p_ug=O.p_article
LEFT JOIN GL_RPU.MANIFCODESLP manif ON O.P_MANIFCODE = manif.itempk 

WHERE enum.CODE='POTENTIAL_MATCHING_FOUND' AND  Sea.code in ('115','315','515','316','516','116')
--AND substr(clp.p_name,30,8) in ('33676937','34515839')


) req

PIVOT (max(p_firstactivated) 

FOR p_name in (
'Transco en cours' as Transco_en_cours,
'Corriger RMS' as Sans_famille_RMS,
'Configuration du workflow' as Choix_Process,
'Attente de stock' as Attente_Stock,
'Validation du Picking' as Eligible_Picking,
'Mise Hors Workflow' as Hors_Workflow,
'Enrichissement de donn�es' as A_Enrichir,
'Attente de l''assignation des medias' as Attente_Visuel,
'Valider l''enrichissement' as A_valider,
'Corriger l''enrichissement' as Correction_Fiche_produit,
'Transco Validee' as Trancode,
'Publication des articles' as Publie)))

) Req21

group by P_UG 

)