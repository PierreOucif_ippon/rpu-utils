select rmsa.p_ug, rmsp.p_ug, cav.p_code
from RMSARTICLES rmsa
left join RMSPRODUCTS rmsp on rmsa.p_product = rmsp.pk
left join OFFERS o on o.p_code = rmsa.p_ug
left join PRODUCTS article on article.pk = o.p_article
left join PRODUCTS product on product.pk = article.P_BASEPRODUCT
left join CAT2PRODREL C2P on C2P.TARGETPK = product.PK
left join CATEGORIES CAT on C2P.SOURCEPK = CAT.PK
left join CATEGORIESLP CATLP on CATLP.itempk = CAT.PK
left join COMPOSEDTYPES ITEMTYPE on ITEMTYPE.pk = CATLP.itemtypepk
left join CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
left join CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
left join CAT2CATREL TOPC2C on TOPC2C.TARGETPK = C2C.SOURCEPK
left join CATEGORIES TOPCAT on TOPCAT.PK = TOPC2C.SOURCEPK
left join CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
left join CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
left join CATEGORIES UNIVCAT on UNIVCAT.PK = UNIVC2C.SOURCEPK
left join CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
left join productfeatures pf on pf.p_product = article.pk
left join CLASSATTRVALUES cav on cav.pk = cast(pf.P_STRINGVALUE as varchar2(3999))
left join CLASSATTRVALUESLP cavlp on cavlp.ITEMPK = cav.pk
left join classvarianttypes cvt on cvt.pk = product.P_CLASSIFICATIONVARIANTTYPE
left join classificationattrs ca on ca.pk = cvt.P_MAINCLASSIFICATIONATTRIBUTE
where UNIVCAT.P_CODE not like 'CL%'and CAT.P_CODE not like '%CLA%' AND PARENTCAT.P_CODE not like '%CLA%'
and UNIVCAT.P_CODE = 'PU_1'
and cav.p_code like concat(ca.p_code,'%')
and pf.p_stringvalue like '8%'
; 