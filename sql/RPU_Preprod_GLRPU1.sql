select distinct pro.code as CodeProduitPCM
    ,PLP.P_NAME Designation
    ,BRANDSLP.P_NAME as marque
    ,Ca.P_Id as Catalog
    ,cv.P_VERSION as Catalog_version
    ,UNIVCATLP.P_NAME Groupe_RMS
    ,PARENTCATLP.P_NAME Famille_RMS
    ,PARENTCAT.p_CODE code_famille_rms
    ,PSSF.UNIVERS
    ,PSSF.code_univers
    ,PSSF.Famille
    ,PSSF.code_fam
    ,PSSF.Sous_famille
    ,PSSF.code_sfam
    ,PSSF.Sous_Sous_Famille
    ,PSSF.code_ssfam
    ,PSSF.ssfam_catalog
    ,PSSF.ssfam_catalog_version

from RMSARTICLES rmsa
        left join RMSPRODUCTS rmsp on rmsp.pk=rmsa.P_PRODUCT
        left join OFFERS o on O.P_CODE=Rmsa.P_UG
        left join products art on o.p_article=art.pk
        left join products pro on art.P_BASEPRODUCT=pro.pk
        left join productslp plp on pro.pk=plp.ITEMPK
        left join ENUMERATIONVALUESLP Saison on Saison.itempk=rmsa.p_season
        left join ENUMERATIONVALUES Sea on Sea.pk=rmsa.p_season
        left join CATEGORIES CAT on rmsp.P_SUBFAMILY=CAT.PK
        left join CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
        left join CATEGORIESLP PARENTCATLP on PARENTCATLP.ITEMPK = C2C.SOURCEPK
        left join CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
        left join CAT2CATREL TOPC2C on TOPC2C.TARGETPK = PARENTCATLP.ITEMPK
        left join CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
        left join CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
        left join CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
        left join BRANDSLP on BRANDSLP.itempk=pro.p_brand
        left join (select rmsa.P_UG as UG_A
                        , rmsa.pk
                        , saison.p_name as SaisonMillesimee
                        , sea2.p_name as SaisonPrecedente
                          from RMSARTICLES rmsa
                          left join RMSARTICLE2RMSSEASON sais2 ON sais2.sourcepk=Rmsa.pk
                          left join ENUMERATIONVALUESLP Sea2 on Sea2.itempk=sais2.targetpk
                          left join ENUMERATIONVALUESLP Saison on Saison.itempk=rmsa.p_season
                          WHERE saison.p_name <> sea2.p_name) Reconduit ON Reconduit.pk=rmsa.pk
       left join CAT2PRODREL UC2P on UC2P.TARGETPK = pro.pk
left join (
 select UUNIVCAT.P_CODE code_univers
      , UUNIVCATLP.P_NAME UNIVERS
      , UPARENTCAT.P_CODE code_sfam
      , UPARENTCATLP.P_NAME Sous_famille
      , UTOPCAT.P_CODE code_fam
      , UTOPCATLP.P_NAME Famille
      , UCAT.P_CODE code_ssfam
      , UCATLP.P_NAME Sous_sous_famille
      , UCAT.Pk PkPSSF
      , ca.P_Id ssfam_catalog
      , cv.P_VERSION ssfam_catalog_version

                from  CATEGORIES UCAT
                inner join CATEGORIESLP UCATLP on UCATLP.itempk = UCAT.PK AND UCATLP.itemtypepk=8796103737426
                inner join CAT2CATREL UC2C on UC2C.TARGETPK = UCAT.PK
                inner join CATEGORIES UPARENTCAT on UPARENTCAT.PK = UC2C.SOURCEPK
                inner join CATEGORIESLP UPARENTCATLP on UPARENTCATLP.ITEMPK = UC2C.SOURCEPK AND UPARENTCATLP.itemtypepk = 8796113666130
                inner join CAT2CATREL UTOPC2C on UTOPC2C.TARGETPK = UPARENTCATLP.ITEMPK
                inner join CATEGORIES UTOPCAT on UTOPCAT.PK = UTOPC2C.SOURCEPK
                inner join CATEGORIESLP UTOPCATLP on UTOPCATLP.ITEMPK = UTOPC2C.SOURCEPK AND UTOPCATLP.itemtypepk = 8796113633362
                inner join CAT2CATREL UUNIVC2C on UUNIVC2C.TARGETPK = UTOPCATLP.ITEMPK
                inner join CATEGORIES UUNIVCAT on UUNIVCAT.PK = UUNIVC2C.SOURCEPK
                Inner Join Categorieslp Uunivcatlp On Uunivcatlp.Itempk = Uunivc2c.Sourcepk And Uunivcatlp.Itemtypepk = 8796113600594
        left join Catalogs ca ON UCAT.P_Catalog = ca.Pk
				left join CatalogVersions cv ON UCAT.p_catalogVersion = cv.pk
) PSSF on PSSF.PkPSSF = UC2P.SOURCEPK
    left join Catalogs ca ON pro.P_Catalog = ca.Pk
    left join CatalogVersions cv ON pro.p_catalogVersion = cv.pk
Where Pssf.Code_Ssfam In ('PSSF247',
'PSSF245',
'PSSF1327',
'PSSF246',
'PSSF750',
'PSSF752',
'PSSF751',
'PSSF450',
'PSSF448',
'PSSF449');
