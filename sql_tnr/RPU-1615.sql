-- rmsArticle.ug
select ewc.P_ARTICLEFORARTICLEDATAEXPORT from ENRICHWFCTX ewc;
select ewc.pk
             , ewc.P_ARTICLEDATAEXPORTDATE
             , ewc.P_GUNDEPOSITDATE
             , ewconf.p_code as code_enrichmentWFConfig
             , rmsa.P_UG as rmsArticle_ug
             , rmsp.P_UG as rmsProduct_ug 
             , rmsa.p_ean as rmsArticle_ean
             , plp.p_name as product_name
             , rmsa.P_SEASON as rmsArticle_season
             , ssq.P_CODE as id_shooting_sequences
             , UNIVCAT.P_CODE code_univers 
             , UNIVCATLP.P_NAME name_UNIVERS
             , TOPCAT.P_CODE code_famille
             , TOPCATLP.P_NAME name_Famille
             , PARENTCATLP.P_NAME name_Sous_famille
             , PARENTCAT.P_CODE code_sous_fam
             , CATLP.P_NAME name_Sous_sous_famille
             , CAT.P_CODE code_sous_sfam
             , brandlp.p_name name_brand
             , cav.P_CODE code_main_classif
             , cavlp.p_name name_main_classif
             , cavv.p_code code_size
             , cavlpp.p_name name_size
             , picking.p_code code_picking
from RMSARTICLES rmsa
join offers o on o.P_CODE = rmsa.P_UG
join RMSPRODUCTS rmsp on rmsp.pk = rmsa.P_PRODUCT
join PRODUCTS article on o.P_ARTICLE = article.pk
join ENRICHWFCTX ewc on ewc.P_ARTICLEFORARTICLEDATAEXPORT = article.pk
join ENRICHWFCONF ewconf on ewconf.pk = ewc.P_ENRICHMENTWORKFLOWCONFIGURAT
join PRODUCTS product on product.pk = article.P_BASEPRODUCT
join PRODUCTSLP plp on plp.ITEMPK = product.pk
join BRANDS brand on brand.pk = product.P_BRAND
left join SHOOTINGSEQUENCES ssq on ssq.pk = product.P_SHOOTINGSEQUENCE
left join BRANDSLP brandlp on brandlp.itempk = brand.pk
left join CAT2PRODREL C2P on C2P.TARGETPK = product.PK
left join CATEGORIES CAT on C2P.SOURCEPK = CAT.PK
left join CATEGORIESLP CATLP on CATLP.itempk = CAT.PK
left join COMPOSEDTYPES ITEMTYPE on ITEMTYPE.pk = CATLP.itemtypepk
left join CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
left join CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
left join CATEGORIESLP PARENTCATLP on PARENTCATLP.ITEMPK = C2C.SOURCEPK
left join CAT2CATREL TOPC2C on TOPC2C.TARGETPK = PARENTCATLP.ITEMPK
left join CATEGORIES TOPCAT on TOPCAT.PK = TOPC2C.SOURCEPK
left join CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
left join CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
left join CATEGORIES UNIVCAT on UNIVCAT.PK = UNIVC2C.SOURCEPK
left join CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
left join productfeatures pf on pf.p_product = article.pk
left join CLASSATTRVALUES cav on cav.pk = cast(pf.P_STRINGVALUE as varchar2(3999))
left join CLASSATTRVALUESLP cavlp on cavlp.ITEMPK = cav.pk
left join classvarianttypes cvt on cvt.pk = product.P_CLASSIFICATIONVARIANTTYPE
left join classificationattrs ca on ca.pk = cvt.P_MAINCLASSIFICATIONATTRIBUTE
left join productfeatures pff on pff.p_product = article.pk
left join CLASSATTRVALUES cavv on cavv.pk = cast(pff.P_STRINGVALUE as varchar2(3999))
left join CLASSATTRVALUESLP cavlpp on cavlpp.ITEMPK = cavv.pk
left join PICKINGS picking on picking.P_RMSARTICLE = rmsa.pk
where ewc.pk = 8800157870945
and UNIVCAT.P_CODE not like 'CL%'and CAT.P_CODE not like '%CLA%' AND PARENTCAT.P_CODE not like '%CLA%'
and cav.p_code like concat(ca.p_code,'%')
and cavv.p_code like ('SIZE%')
and ROWNUM = 1
order by picking.createdts;

select * from SUPPLIERIMAGE si
where si.pk =8797142723431;

select ewc.P_SUPPLIERIMAGES from ENRICHWFCTX ewc
where ewc.pk = 8796101487457;

--Reset ArticleExportDate
update ENRICHWFCTX set P_ARTICLEDATAEXPORTDATE = NULL
where pk = 8800157870945;

--Reset SupplierImagesExportDate
update ENRICHWFCTX set P_GUNDEPOSITDATE = NULL
where pk = 8800157870945;

update SUPPLIERIMAGE 
set P_GUNDEPOSITDATE = null, P_EXPORTDATE = null
where pk =8796093295463;






select ewc.pk
             , ewc.P_ARTICLEDATAEXPORTDATE
             , ewc.P_GUNDEPOSITDATE
             , ewconf.p_code as code_enrichmentWFConfig
             , rmsa.P_UG as rmsArticle_ug
             , rmsp.P_UG as rmsProduct_ug 
             , rmsa.p_ean as rmsArticle_ean
             , plp.p_name as product_name
             , rmsa.P_SEASON as rmsArticle_season
             , ssq.P_CODE as id_shooting_sequences
             , UNIVCAT.P_CODE code_univers 
             , UNIVCATLP.P_NAME name_UNIVERS
from RMSARTICLES rmsa
join offers o on o.P_CODE = rmsa.P_UG
join RMSPRODUCTS rmsp on rmsp.pk = rmsa.P_PRODUCT
join PRODUCTS article on o.P_ARTICLE = article.pk
join ENRICHWFCTX ewc on ewc.P_ARTICLEFORARTICLEDATAEXPORT = article.pk
join ENRICHWFCONF ewconf on ewconf.pk = ewc.P_ENRICHMENTWORKFLOWCONFIGURAT
join PRODUCTS product on product.pk = article.P_BASEPRODUCT
join PRODUCTSLP plp on plp.ITEMPK = product.pk
join BRANDS brand on brand.pk = product.P_BRAND
left join SHOOTINGSEQUENCES ssq on ssq.pk = product.P_SHOOTINGSEQUENCE
left join BRANDSLP brandlp on brandlp.itempk = brand.pk
left join CAT2PRODREL C2P on C2P.TARGETPK = product.PK
left join CATEGORIES CAT on C2P.SOURCEPK = CAT.PK
left join CATEGORIESLP CATLP on CATLP.itempk = CAT.PK
left join COMPOSEDTYPES ITEMTYPE on ITEMTYPE.pk = CATLP.itemtypepk
left join CAT2CATREL C2C on C2C.TARGETPK = CAT.PK
left join CATEGORIES PARENTCAT on PARENTCAT.PK = C2C.SOURCEPK
left join CATEGORIESLP PARENTCATLP on PARENTCATLP.ITEMPK = C2C.SOURCEPK
left join CAT2CATREL TOPC2C on TOPC2C.TARGETPK = PARENTCATLP.ITEMPK
left join CATEGORIES TOPCAT on TOPCAT.PK = TOPC2C.SOURCEPK
left join CATEGORIESLP TOPCATLP on TOPCATLP.ITEMPK = TOPC2C.SOURCEPK
left join CAT2CATREL UNIVC2C on UNIVC2C.TARGETPK = TOPCATLP.ITEMPK
left join CATEGORIES UNIVCAT on UNIVCAT.PK = UNIVC2C.SOURCEPK
left join CATEGORIESLP UNIVCATLP on UNIVCATLP.ITEMPK = UNIVC2C.SOURCEPK
where UNIVCAT.P_CODE not like 'CL%'and CAT.P_CODE not like '%CLA%' AND PARENTCAT.P_CODE not like '%CLA%';
