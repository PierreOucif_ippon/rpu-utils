#!/usr/bin/perl

use strict;

use DBI;
use DBD::Oracle;
use Switch;
use Getopt::Std;
use Data::Dumper;


my %options;
#get and check opt
getopts( "b:r:h",\%options);
print_usage() if ($options{h});


#Fonction Usage
sub print_usage {
        print "Usage :\n";
        print "-b : se connecte a hybris\n";
        print "-r : se connecte a RMS\n";
        exit(2);
}


sub connectHybris {
        my ($SID, $login, $passwd) = qw(GL GL_HYBRIS GbeYJqZL);
        return DBI->connect( "dbi:Oracle:$SID", $login, $passwd ) || die( $DBI::errstr . "\n" );
}

sub connectRMS {
        my ($SID, $login, $passwd) = qw(GOEJ user_gl user_gl);
        return DBI->connect( "dbi:Oracle:$SID", $login, $passwd ) || die "Erreur de connection à la base e                Nova" ;
}


my $SQL = "$ARGV[0]";
my $entete;
my $bool;
if ($options{r})
{
   my $dbh = connectRMS();
}
my $dbh = connectHybris();
$dbh->{LongReadLen} = 81920;
my $req=$dbh->prepare($SQL);
$req->execute;

print join(";", @{$req->{NAME}})."\n" if ($ARGV[1] eq "-h" );
while (my @row = $req->fetchrow_array) {
    print join(";", @row), "\n";
} 
